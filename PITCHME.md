@snap[north]
@size[80%](<h2> Image </h2>)
@snapend
[Logo](assets/img/try.svg)

---
@snap[west]
@size[80%](<h2> Vector, Covector, the Exterior Derivative and **HashedExpresion** </h2>)
@snapend

---
@snap[north]
@size[80%](<h2> Type </h2>)
@snapend

```haskell
data Expression d et = Expression Int ExpressionMap

type ExpressionMap = IntMap Node

type Node 
    = Var name 
    | DVar name
    | Sum ET Args

```

